# Desafio Dados
Bem vindo(a)! Esse desafio tem como objetivo avaliar a capacidade do(a) candidato(a) de construir um pipeline de dados.

## O Desafio
O pipeline deverá conter uma etapa de extração de dados, uma etapa de geração de métricas e uma última etapa de exibição das métricas em uma dashboard.

**Será avaliada a capacidade do candidato para escrever um pipeline com código legível.**

## Cenário
Uma empresa do mercado financeiro precisa visualizar os informes diários dos fundos de investimento do mercado. A CVM disponibiliza planilhas com os informes de cada mês do último ano na url http://dados.cvm.gov.br/dataset/fi-doc-inf_diario.

A partir das planilhas, precisamos de um dashboard com três gráficos com o crescimento das seguintes informações de um determinado fundo:
- Valor Patrimonial Líquido
- Valor Cota
- Rendimento => calculado a partir do valor patrimonial líquido do dia interior, levando em conta os valores de captação e de resgate.

O dashboard deve exibir um filtro para a seleção do fundo desejado para visualização pelo usuário.


## Entregáveis
- Extração dos dados: código responsável por extrair as planilhas. Por exemplo: script em python, script em selenium.
- Geração das métricas: código responsável por gerar as métricas. Por exemplo: pandas, numpy, pyspark.
- Dashboards: código ou plataforma responsável por gerar os dashboard com os gráficos. Por exemplo: Streamlit, Ploty Dash, Apache Superset.

## Avaliação
Você será avaliado nos seguintes aspectos, em ordem de prioridade:
1. Performance e correta execução da especificação funcional
2. Legibilidade de código e consistência de nomenclaturas
2. Cálculos corretos
4. Documentação
5. Completar todas funcionalidades

É esperado que você desenvolva sem ajuda ou intervenção direta de terceiros, mas encorajamos que você pesquise por soluções e boas práticas sem nenhum tipo de restrição, apenas lembre-se que serão realizadas perguntas na entrevista a fim de certificar seu conhecimento total sobre a implementação. **Jogue limpo!**


## Considerações
-   Você deverá criar um fork privado do projeto e compartilhar conosco. Por favor, **não** abra um merge request público.
-   O desafio deverá ser entregue em até uma semana. Se você não conseguir fazer tudo, não deixe de entregar. Se você empacar em alguma parte, nos procure e explique o que está acontecendo.
-   Faça commits atômicos e semânticos.
